# WordPress mit Docker

Gebraucht wird: Eine native [Docker-Beta](https://www.docker.com).

## Entwicklungsumgebung
*install.sh*
Installiert eine Entwicklungsumgebung.
WordPress-, MySQL-, Node.js- und Busybox-Images werden gegebenenfalls geladen.
Die WordPress-Installation und die Datenbank werden in Busybox-Images persistiert.
Das zu bearbeitende Theme muss vor dem Install in den Ordner _ms-theme_ kopiert oder ausgecheckt werden.
Im Node-Container wird ein _npm-install_ und _bower_install_ ausgeführt.

*start.sh*
startet die Container der Entwicklungsumgebung.

*gulp.sh*
führt _gulp_ im Node-Container aus.

*stop.sh*
stoppt die Container der Entwicklungsumgebung.

*delete.sh*
löscht die Container der Entwicklungsumgebung.

*delete-data.sh*
löscht auch die Daten-Container der Entwicklungsumgebung.