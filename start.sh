#!/usr/bin/env bash

echo "=== Starte MySQL-Container"
docker start ms-mysql

echo "=== Starte WordPress-Container"
docker start ms-wordpress

echo "=== Starte Gulp im node.js-Container"
docker start ms-node

echo "=== Viel Spaß!"
