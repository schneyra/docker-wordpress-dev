#!/usr/bin/env bash

echo "=== Stoppe WordPress-Container"
docker stop ms-wordpress

echo "=== Stoppe MySQL-Container"
docker stop ms-mysql

echo "=== Stoppe Node-Container"
docker stop ms-node

echo "=== Tschö!"
