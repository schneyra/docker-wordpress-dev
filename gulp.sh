#!/usr/bin/env bash

# executes gulp-tasks in node-container
docker exec -ti ms-node node_modules/.bin/gulp $1
