#!/usr/bin/env bash

echo "=== Lösche MySQL-Container"
docker rm ms-mysql

echo "=== Lösche WordPress-Container"
docker rm ms-wordpress

echo "=== Lösche node.js-Container"
docker rm ms-node

echo "=== Tschö!"
