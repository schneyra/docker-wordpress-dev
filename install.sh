#!/usr/bin/env bash

echo "=== Installiere und starte MySQL-Container"
docker run --name ms-data-mysql -v /var/lib/mysql busybox
docker run --name ms-mysql --volumes-from ms-data-mysql -e MYSQL_ROOT_PASSWORD=rootpass -d mysql

echo "=== Installiere und starte WordPress-Container"
docker run --name ms-data-wordpress -v /var/www/html busybox
docker run --name ms-wordpress --link ms-mysql:mysql -p 80:80 --volumes-from ms-data-wordpress -v "$PWD/ms-theme":/var/www/html/wp-content/themes/ms-theme -d wordpress

echo "=== Installiere und starte node.js-Container"
docker run -ti --name ms-node --link ms-wordpress:wordpress.local -p 3000:3000 -p 3001:3001 -v "$PWD/ms-theme":/usr/src -w /usr/src -d node

echo "=== npm install & bower install"
docker exec -ti ms-node npm install
docker exec -ti ms-node node_modules/.bin/bower install --allow-root --config.interactive=false

echo "=== Viel Spaß!"
